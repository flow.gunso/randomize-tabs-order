# !/bin/bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# Install required packages.
apk add curl
npm install

web-ext sign \
    --config package.json \
    --source-dir randomize-tabs-order/ \
    --api-key $CI_MOZILLA_API_KEY \
    --api-secret $CI_MOZILLA_API_SECRET
