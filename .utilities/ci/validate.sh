# !/bin/bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

function fail_with_message {
    echo "$1"
    exit 1
}

if [[ -z "$CI_MOZILLA_API_KEY" ]]; then fail_with_message "Mozilla API key is not defined."; fi
if [[ -z "$CI_MOZILLA_API_SECRET" ]]; then fail_with_message "Mozilla API secret is not defined."; fi
