/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

function getRandomIndex(max_index){
    return Math.floor(Math.random() * Math.floor(max_index))
}

function randomizeTabs(tabs) {
    max_index = tabs.length;
    for (let tab of tabs) {
        browser.tabs.move(tab.id, {index: getRandomIndex(max_index)});
    }
}

function entrypoint() {
    browser.tabs.query({currentWindow: true})
        .then(randomizeTabs);
}

browser.browserAction.onClicked.addListener(entrypoint);
